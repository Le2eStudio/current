package com.le2e.current.ui.common.list_utils.source_list;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.le2e.current.R;

public class SourceIconViewHolder extends RecyclerView.ViewHolder {

    public ImageView sourceIcon;

    public SourceIconViewHolder(View itemView) {
        super(itemView);
        sourceIcon = (ImageView) itemView.findViewById(R.id.sliding_panel_source_list_icon);
    }
}
