package com.le2e.current.data.manager.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.le2e.current.data.model.db.columns.LastViewedArticleTableColumns;
import com.le2e.current.data.model.db.columns.ProvisionalTableColumns;
import com.le2e.current.data.model.db.columns.SavedArticlesTableColumns;
import com.le2e.current.data.model.db.columns.SourcesTableColumns;

public class DBHelper extends SQLiteOpenHelper{
    private static final String DATABASE_NAME = "Current.db";
    private static final int DATABASE_VERSION = 2;
    private final Context context;

    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + SourcesTableColumns.TABLE + " ("
                + SourcesTableColumns._ROW_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + SourcesTableColumns.SOURCE_ID + " TEXT, "
                + SourcesTableColumns.SOURCE_NAME + " TEXT, "
                + SourcesTableColumns.SOURCE_DESCRIPTION + " TEXT, "
                + SourcesTableColumns.SOURCE_URL + " TEXT, "
                + SourcesTableColumns.SOURCE_CATEGORY + " TEXT, "
                + SourcesTableColumns.SOURCE_LANGUAGE + " TEXT, "
                + SourcesTableColumns.SOURCE_COUNTRY + " TEXT, "
                + SourcesTableColumns.SOURCE_LOGO_URL + " TEXT "
                + ");");

        db.execSQL("CREATE TABLE " + ProvisionalTableColumns.TABLE + " ("
                + ProvisionalTableColumns._ARTICLE_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + ProvisionalTableColumns.AUTHOR + " TEXT, "
                + ProvisionalTableColumns.SOURCE_ID + " TEXT, "
                + ProvisionalTableColumns.ARTICLE_DESCRIPTION + " TEXT, "
                + ProvisionalTableColumns.ARTICLE_TITLE + " TEXT, "
                + ProvisionalTableColumns.ARTICLE_URL + " TEXT, "
                + ProvisionalTableColumns.ARTICLE_IMAGE_URL + " TEXT, "
                +ProvisionalTableColumns.PUBLISHED_AT + " TEXT "
                + ");");

        db.execSQL("CREATE TABLE " + LastViewedArticleTableColumns.TABLE + " ("
                + LastViewedArticleTableColumns.SOURCE_URL + " TEXT, "
                + LastViewedArticleTableColumns.AUTHOR + " TEXT, "
                + LastViewedArticleTableColumns.ARTICLE_DESCRPTION + " TEXT, "
                + LastViewedArticleTableColumns.ARTICLE_TITLE + " TEXT, "
                + LastViewedArticleTableColumns.ARTICLE_URL + " TEXT, "
                + LastViewedArticleTableColumns.ARTICLE_IMAGE_URL + " TEXT, "
                + LastViewedArticleTableColumns.PUBLISHED_AT + " TEXT, "
                + LastViewedArticleTableColumns.SOURCE_LOGO_URL + " TEXT "
                + ");");

        db.execSQL("CREATE TABLE " + SavedArticlesTableColumns.TABLE + " ("
                + SavedArticlesTableColumns._SAVED_ARTILCE_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + SavedArticlesTableColumns.SAVED_TITLE + " TEXT, "
                + SavedArticlesTableColumns.SAVED_DESCRIPTION + " TEXT, "
                + SavedArticlesTableColumns.SAVED_URL + " TEXT, "
                + SavedArticlesTableColumns.SAVED_SOURCE_LOGO_URL + " TEXT, "
                + SavedArticlesTableColumns.SAVED_SOURCE_URL + " TEXT, "
                + SavedArticlesTableColumns.SAVED_CATEGORY + " TEXT, "
                + SavedArticlesTableColumns.SAVED_PUBLISHED_AT + " TEXT "
                + ");");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO replace this logic with actual update logic
        db.execSQL("DROP TABLE IF EXISTS " + SourcesTableColumns.TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + ProvisionalTableColumns.TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + LastViewedArticleTableColumns.TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + SavedArticlesTableColumns.TABLE);
        onCreate(db);
    }

    public void clearAll(SQLiteDatabase db){
        db.execSQL("DELETE FROM " + SourcesTableColumns.TABLE);
        db.execSQL("DELETE FROM " + ProvisionalTableColumns.TABLE);
        db.execSQL("DELETE FROM " + LastViewedArticleTableColumns.TABLE);
        db.execSQL("DELETE FROM " + SavedArticlesTableColumns.TABLE);
    }

    public void clearSources(SQLiteDatabase db){
        db.execSQL("DELETE FROM " + SourcesTableColumns.TABLE);
    }

    public void clearProvisions(SQLiteDatabase db){
        db.execSQL("DELETE FROM " + ProvisionalTableColumns.TABLE);
    }

    public void clearLastViewed(SQLiteDatabase db){
        db.execSQL("DELETE FROM " + LastViewedArticleTableColumns.TABLE);
    }

    public void clearSaved(SQLiteDatabase db){
        db.execSQL("DELETE FROM " + SavedArticlesTableColumns.TABLE);
    }
}
