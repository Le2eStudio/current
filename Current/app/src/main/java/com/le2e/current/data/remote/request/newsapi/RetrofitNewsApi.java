package com.le2e.current.data.remote.request.newsapi;

import com.le2e.current.data.remote.response.newsapi.ArticlesResponse;
import com.le2e.current.data.remote.response.newsapi.SourceResponse;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;
import timber.log.Timber;

public interface RetrofitNewsApi {

    @GET("/{version}/sources")
    Observable<SourceResponse> obsGetSources(@Path("version") String version, @Query("category") String category, @Query("language") String language, @Query("country") String country);

    @GET("/{version}/articles")
    Observable<ArticlesResponse> obsGetArticles(@Path("version") String version, @Query("source") String source, @Query("apiKey") String apiKey, @Query("sortBy") String sortBy);

    class Factory {
        private static RetrofitNewsApi service;

        static RetrofitNewsApi getRxInstance() {
            if (service == null) {
                Retrofit retrofit = new Retrofit.Builder()
                        .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                        .addConverterFactory(GsonConverterFactory.create())
                        .baseUrl(HttpConstants.BASE_URL)
                        .build();

                service = retrofit.create(RetrofitNewsApi.class);
            }

            return service;
        }
    }
}
