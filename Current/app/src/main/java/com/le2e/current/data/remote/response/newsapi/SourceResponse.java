package com.le2e.current.data.remote.response.newsapi;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class SourceResponse {
    @SerializedName("status")
    private String status;
    @SerializedName("sources")
    private ArrayList<SourceDetails> sources;

    public String getStatus() {
        return status;
    }

    public ArrayList<SourceDetails> getSources() {
        return sources;
    }
}
