package com.le2e.current.data.manager;


import com.le2e.current.data.local.db.ProvisionalDBAdapter;
import com.le2e.current.data.local.db.SourceDBAdapter;
import com.le2e.current.data.manager.db.DatabaseManager;
import com.le2e.current.data.model.db.ProvisionalDBO;
import com.le2e.current.data.model.db.SourceDBO;
import com.le2e.current.data.remote.request.newsapi.ApiContentHelper;
import com.le2e.current.data.remote.response.newsapi.ArticleDetails;
import com.le2e.current.data.remote.response.newsapi.ArticlesResponse;
import com.le2e.current.data.remote.response.newsapi.SourceDetails;
import com.le2e.current.data.remote.response.newsapi.SourceResponse;
import com.le2e.current.ui.loading_screen.PresenterImpl;

import java.util.ArrayList;
import java.util.HashMap;

import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;
import timber.log.Timber;

public class DataManager {

    private Subscription fulLoadSub;
    private Subscription provFromDBSub;
    private Subscription sourceLogoSub;

    private SourceDBAdapter sourceDBAdapter;
    private ProvisionalDBAdapter provisionalDBAdapter;
    private ApiContentHelper apiHelper;
    private PresenterImpl presenter;

    private boolean isDoingFullLoad = false;
    private boolean isGettingDBProvisionals = false;
    private boolean isGettingLogos = false;

    private ArrayList<SourceDBO> sourceDBOList;
    private ArrayList<ProvisionalDBO> provisionalDBOList;

    private String id = "";

    public DataManager(PresenterImpl presenter) {
        this.presenter = presenter;
        apiHelper = new ApiContentHelper();
        sourceDBAdapter = DatabaseManager.getInstance().getSourceDBAdapter();
        provisionalDBAdapter = DatabaseManager.getInstance().getProvisionalDBAdapter();
    }

    // ***********************************************************************************
    // ********************************** DATA REQUESTS **********************************
    // ***********************************************************************************

    // starts full load sequence by clearing source/provisional tables in database then calling api
    public void startSourceFullLoadSequence() {
        clearSources(); // chains to clearProvisionals()
    }

    // load provisionals from database, returns them to presenter.provisionalRequestSuccess(ArrayList<ProvisionalDBO>)
    public void requestProvisionalsFromDB() {
        loadProvisionalsFromDB();
    }

    // load source logo urls from api, returns them to presenter.sourceLogoRequestSuccess(HashMap<id, url>)
    public void requestSourceLogos() {
        getSourceLogosFromDB();
    }

    // ***********************************************************************************
    // *********************************** OBSERVABLES ***********************************
    // ***********************************************************************************

    // get sources/all articles from api and store them in database
    private void fullLoad() {
        if(!isDoingFullLoad) {
            isDoingFullLoad = true;

            fulLoadSub = apiHelper.getSourcesFromApi()
                    .flatMap(new Func1<SourceResponse, Observable<ArrayList<SourceDetails>>>() {
                        @Override
                        public Observable<ArrayList<SourceDetails>> call(SourceResponse sourceResponse) {
                            return Observable.just(sourceResponse.getSources()); // make api call to get source response
                        }
                    })
                    .flatMap(new Func1<ArrayList<SourceDetails>, Observable<SourceDetails>>() {
                        @Override
                        public Observable<SourceDetails> call(ArrayList<SourceDetails> detailList) {
                            return Observable.from(detailList); // picks out each source detail
                        }
                    })
                    .flatMap(new Func1<SourceDetails, Observable<String>>() {
                        @Override
                        public Observable<String> call(SourceDetails sourceDetails) {
                            saveSourceDetails(sourceDetails); // save each source to sourceTable
                            return Observable.just(sourceDetails.getId()); // serve up id for articles per source call
                        }
                    })
                    .flatMap(new Func1<String, Observable<ArticlesResponse>>() {
                        @Override
                        public Observable<ArticlesResponse> call(String sId) {
                            return apiHelper.getArticlesFromApi(sId);
                        }
                    })
                    .doOnNext(new Action1<ArticlesResponse>() {
                        @Override
                        public void call(ArticlesResponse articlesResponse) {
                            id = articlesResponse.getSource();
                        }
                    })
                    .flatMap(new Func1<ArticlesResponse, Observable<ArrayList<ArticleDetails>>>() {
                        @Override
                        public Observable<ArrayList<ArticleDetails>> call(ArticlesResponse articlesResponse) {
                            return Observable.just(articlesResponse.getArticles());
                        }
                    })
                    .flatMap(new Func1<ArrayList<ArticleDetails>, Observable<ArticleDetails>>() {
                        @Override
                        public Observable<ArticleDetails> call(ArrayList<ArticleDetails> articleList) {
                            return Observable.from(articleList);
                        }
                    })
                    .flatMap(new Func1<ArticleDetails, Observable<ProvisionalDBO>>() {
                        @Override
                        public Observable<ProvisionalDBO> call(ArticleDetails details) {
                            details.setSourceId(id);
                            return provisionalDBAdapter.insertProvisionalToDBObserve(details); // save article to db
                        }
                    })
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Subscriber<ProvisionalDBO>() {
                        @Override
                        public void onCompleted() {
                            Timber.d("Full load complete");
                            isDoingFullLoad = false;
                            presenter.fullLoadSuccess();
                        }

                        @Override
                        public void onError(Throwable e) {
                            Timber.d(e, "Full load error");
                            presenter.fullLoadFail(e);
                        }

                        @Override
                        public void onNext(ProvisionalDBO provisionalDBO) {
                            Timber.d("***************************New Article***************************");
                            Timber.d(provisionalDBO.details.getTitle());
                            Timber.d(provisionalDBO.details.getUrl());
                            Timber.d(provisionalDBO.details.getSourceId());
                            Timber.d(provisionalDBO.details.getAuthor());
                            Timber.d(provisionalDBO.details.getDescription());
                        }
                    });
        } else {
            Timber.d("Prevented multiple full load requests");
        }
    }

    // gets all provisional articles from database
    private void loadProvisionalsFromDB() {
        if (!isGettingDBProvisionals) {
            isGettingDBProvisionals = true;

            // subscribe to db
            provFromDBSub = provisionalDBAdapter.getAllProvisionalArticlesObserve()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Subscriber<ArrayList<ProvisionalDBO>>() {
                        @Override
                        public void onCompleted() {
                            isGettingDBProvisionals = false;
                            presenter.provisionalRequestSuccess(provisionalDBOList);
                        }

                        @Override
                        public void onError(Throwable e) {
                            e.printStackTrace();
                            isGettingDBProvisionals = false;
                            presenter.provisionalRequestFail();
                        }

                        @Override
                        public void onNext(ArrayList<ProvisionalDBO> provisionalDBOs) {
                            provisionalDBOList = provisionalDBOs;
                        }
                    });

        } else {
            Timber.d("Prevented multiple loads of provisionals");
        }
    }

    // gets source logo urls from database
    private void getSourceLogosFromDB() {
        if (!isGettingLogos) {
            isGettingLogos = true;

            sourceLogoSub = sourceDBAdapter.getLogosObserve()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Subscriber<HashMap<String, String>>() {
                        @Override
                        public void onCompleted() {

                        }

                        @Override
                        public void onError(Throwable e) {
                            presenter.sourceLogoRequestFail();
                        }

                        @Override
                        public void onNext(HashMap<String, String> sourceLogos) {
                            presenter.sourceLogoRequestSuccess(sourceLogos);
                        }
                    });
        } else {
            Timber.d("Prevented multiple logo queries");
        }
    }

    // saves source details to database in full load sequence
    private void saveSourceDetails(SourceDetails details) {
        sourceDBAdapter.insertSourceToDBObserve(details)
                .subscribeOn(Schedulers.newThread())
                .subscribe();
    }

    // clear our provisional database table in full load sequence
    private void clearProvisionals() {
        provisionalDBAdapter.clearProvisionalTableObserve()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<Boolean>() {
                    @Override
                    public void call(Boolean aBoolean) {
                        fullLoad();
                        Timber.d("Provisional table cleared");
                    }
                });
    }

    // clear out source database table in full load sequence
    private void clearSources() {
        sourceDBAdapter.clearSourceTableObserve()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<Boolean>() {
                    @Override
                    public void call(Boolean aBoolean) {
                        clearProvisionals();
                        Timber.d("Source table cleared");
                    }
                });
    }

    // ***********************************************************************************
    // ************************************ UTILITIES ************************************
    // ***********************************************************************************

    public void unSubscribeObservables() {
        if (sourceLogoSub != null && !sourceLogoSub.isUnsubscribed())
            sourceLogoSub.unsubscribe();

        if (provFromDBSub != null && !provFromDBSub.isUnsubscribed())
            provFromDBSub.unsubscribe();

        if(fulLoadSub != null && !fulLoadSub.isUnsubscribed())
            fulLoadSub.unsubscribe();

        resetPipeline();
    }

    private void resetPipeline() {
        isDoingFullLoad = false;
        isGettingDBProvisionals = false;
        isGettingLogos = false;

        if (sourceDBOList != null)
            sourceDBOList.clear();

        if (provisionalDBOList != null)
            provisionalDBOList.clear();
    }
}
