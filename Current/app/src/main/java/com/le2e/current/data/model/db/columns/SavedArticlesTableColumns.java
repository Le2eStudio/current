package com.le2e.current.data.model.db.columns;

public class SavedArticlesTableColumns {
    public static final String TABLE = "SavedArticlesTable";
    public static final String _SAVED_ARTILCE_ID = "SavedArticleId";
    public static final String SAVED_TITLE = "SavedTitle";
    public static final String SAVED_DESCRIPTION = "SavedDescription";
    public static final String SAVED_URL = "SavedUrl";
    public static final String SAVED_SOURCE_LOGO_URL = "SavedSourceLogoUrl";
    public static final String SAVED_SOURCE_URL = "SavedSourceUrl";
    public static final String SAVED_CATEGORY = "SavedCategory";
    public static final String SAVED_PUBLISHED_AT = "SavedPublishedAt";
}
