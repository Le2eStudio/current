package com.le2e.current.ui.common.list_utils.article_list;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.le2e.current.R;

public class ArticleDetailViewHolder extends RecyclerView.ViewHolder {
    public ImageView logo;
    public ImageView image;
    public TextView publishedDate;
    public TextView title;
    public TextView author;
    public TextView description;

    public ArticleDetailViewHolder(View itemView) {
        super(itemView);

        logo = (ImageView)itemView.findViewById(R.id.article_card_source_logo);
        image = (ImageView)itemView.findViewById(R.id.article_card_image);
        publishedDate = (TextView)itemView.findViewById(R.id.article_card_published);
        title = (TextView)itemView.findViewById(R.id.article_card_title);
        author = (TextView)itemView.findViewById(R.id.article_card_author);
        description = (TextView)itemView.findViewById(R.id.article_card_description);

    }
}
