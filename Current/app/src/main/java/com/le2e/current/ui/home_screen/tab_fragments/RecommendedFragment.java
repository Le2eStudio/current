package com.le2e.current.ui.home_screen.tab_fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.le2e.current.config.BaseApplication;
import com.le2e.current.ui.common.list_utils.article_list.ArticleListAdapter;
import com.le2e.current.ui.home_screen.tab_fragments.presenters.RecommendedTabPresenter;
import com.le2e.current.R;

import javax.inject.Inject;

public class RecommendedFragment extends Fragment {
    @Inject
    RecommendedTabPresenter presenter;

    private RecyclerView articleRecyclerView;
    private LinearLayoutManager manager;
    private ArticleListAdapter adapter;

    public RecommendedFragment() {

    }

    public static RecommendedFragment newInstance() {
        RecommendedFragment fragment = new RecommendedFragment();
        // put bundle save logic here
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((BaseApplication) getActivity().getApplication()).getTabFragmentComponent().inject(this);
        Log.d("FragmentLog", "recommended created");
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Log.d("FragmentLog", "recommended attached");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_test_recommended, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupViews(view);
        presenter.attachFragment(this);
        presenter.load(articleRecyclerView, adapter);
        Log.d("FragmentLog", "recommended view created");
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.attachFragment(this);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    private void setupViews(final View view) {
        articleRecyclerView = (RecyclerView) view.findViewById(R.id.recommended_fragment_article_list);
        articleRecyclerView.setHasFixedSize(true);
        manager = new LinearLayoutManager(getActivity());
        articleRecyclerView.setLayoutManager(manager);
        adapter = new ArticleListAdapter(getActivity(), presenter);
        //adapter.setHasStableIds(true);
        articleRecyclerView.setAdapter(adapter);

    }

    @Override
    public void onDestroyView() {
        presenter.detachFragment();
        super.onDestroyView();
        Log.d("FragmentLog", "recommended frag destroyed");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d("FragmentLog", "recommended frag paused");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d("FragmentLog", "recommended frag stopped");
    }
}
