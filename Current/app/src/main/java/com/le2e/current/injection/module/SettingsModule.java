package com.le2e.current.injection.module;

import com.le2e.current.ui.settings_screen.SettingsActivityPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class SettingsModule {
    @Provides
    SettingsActivityPresenter providesSettingsPresenter() {
        return new SettingsActivityPresenter();
    }
}
