package com.le2e.current.data.remote.response.newsapi;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class SourceDetails {
    @SerializedName("id")
    private String id;
    @SerializedName("name")
    private String name;
    @SerializedName("description")
    private String description;
    @SerializedName("url")
    private  String url;
    @SerializedName("category")
    private String category;
    @SerializedName("language")
    private String language;
    @SerializedName("country")
    private String country;
    @SerializedName("urlsToLogos")
    private urlsToLogos UrlsToLogos;
    @SerializedName("sortBysAvailable")
    private ArrayList<String> sortBysAvailable;

    private String logoUrl;

    public SourceDetails(){

    }

    public SourceDetails(String id, String name, String description, String url, String category, String language, String country, String logoURL) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.url = url;
        this.category = category;
        this.language = language;
        this.country = country;
        this.logoUrl = logoURL;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getUrl() {
        return url;
    }

    public String getCategory() {
        return category;
    }

    public String getLanguage() {
        return language;
    }

    public String getCountry() {
        return country;
    }

    public String getLogoUrl() {
        return logoUrl;
    }

    public urlsToLogos getUrlsToLogos() {
        return UrlsToLogos;
    }

    public ArrayList<String> getSortBysAvailable() {
        return sortBysAvailable;
    }
}
