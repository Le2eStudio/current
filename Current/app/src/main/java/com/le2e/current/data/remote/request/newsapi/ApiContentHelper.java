package com.le2e.current.data.remote.request.newsapi;


import com.le2e.current.data.remote.response.newsapi.ArticlesResponse;
import com.le2e.current.data.remote.response.newsapi.SourceDetails;
import com.le2e.current.data.remote.response.newsapi.SourceResponse;

import java.util.ArrayList;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func0;
import rx.functions.Func1;
import rx.schedulers.Schedulers;
import timber.log.Timber;

public class ApiContentHelper {
    private RetrofitNewsApi newsApi;

    private String sortBy;
    private String category;
    private String language;
    private String country;

    public ApiContentHelper() {
        newsApi = RetrofitNewsApi.Factory.getRxInstance();
    }

    public Observable<SourceResponse> getSourcesFromApi() {
        return Observable.defer(new Func0<Observable<SourceResponse>>() {
            @Override
            public Observable<SourceResponse> call() {
                getUserSourcePreferences();
                Timber.d("Calling api");
                return newsApi.obsGetSources(HttpConstants.NEWS_API_VERSION, category, language, country);
            }
        });
    }

    public void query(){

    }

    private void getUserSourcePreferences() {
        // add logic to get shared preferences
        Timber.d("User preferences retrieved");
        category = "";
        language = "en";
        country = "";
        sortBy = "";
    }

    public Observable<ArticlesResponse> getArticlesFromApi(final String sourceId) {
        return Observable.defer(new Func0<Observable<ArticlesResponse>>() {
            @Override
            public Observable<ArticlesResponse> call() {
                return newsApi.obsGetArticles(HttpConstants.NEWS_API_VERSION, sourceId, HttpConstants.NEWS_API_KEY, sortBy);
            }
        });
    }
}
