package com.le2e.current.data.model.db.columns;

public class ProvisionalTableColumns {
    public static final String TABLE = "ProvisionalArticlesTable";
    public static final String _ARTICLE_ID = "ArticleId";
    public static final String SOURCE_ID = "SourceId";
    public static final String AUTHOR = "Author";
    public static final String ARTICLE_DESCRIPTION = "ArticleDescription";
    public static final String ARTICLE_TITLE = "ArticleTitle";
    public static final String ARTICLE_URL = "ArticleUrl";
    public static final String ARTICLE_IMAGE_URL = "ArticleImageUrl";
    public static final String PUBLISHED_AT = "PublishedAt";
}
