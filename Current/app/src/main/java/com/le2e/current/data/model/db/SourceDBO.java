package com.le2e.current.data.model.db;

import com.le2e.current.data.remote.response.newsapi.SourceDetails;

public class SourceDBO {
    private Long rowId;
    private SourceDetails details;

    public SourceDBO(Long rowId, SourceDetails details){
        this.rowId = rowId;
        this.details = details;
    }

    public Long getRowId() {
        return rowId;
    }

    public SourceDetails getDetails() {
        return details;
    }
}
