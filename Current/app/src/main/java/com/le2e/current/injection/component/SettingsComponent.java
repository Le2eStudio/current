package com.le2e.current.injection.component;

import com.le2e.current.injection.module.SettingsModule;
import com.le2e.current.ui.settings_screen.SettingsActivity;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component (modules = {SettingsModule.class})
public interface SettingsComponent {
    void inject (SettingsActivity activity);
}
