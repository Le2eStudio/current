package com.le2e.current.ui.loading_screen;

import com.le2e.current.data.model.db.ProvisionalDBO;
import com.le2e.current.data.model.db.SourceDBO;
import com.le2e.current.ui.base.DataManagerPresenter;

import java.util.ArrayList;
import java.util.HashMap;

public interface PresenterImpl extends DataManagerPresenter {
    void sourceLoadSuccess();
    void articleLoadSuccess();

    void getNewsSourceFail();
    void saveNewsSourceFail();
    void getArticlesFail();
    void saveArticlesFail();

    void sourceRequestSuccess(ArrayList<SourceDBO> sourceDBOs);
    void sourceRequestFail();

    void provisionalRequestSuccess(ArrayList<ProvisionalDBO> provisionalDBOs);
    void provisionalRequestFail();

    void sourceLogoRequestSuccess(HashMap<String, String> logoList);
    void sourceLogoRequestFail();
}
