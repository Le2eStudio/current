package com.le2e.current.data.model.db.columns;

public class LastViewedArticleTableColumns {
    public static final String TABLE = "LastViewArticleTable";
    public static final String SOURCE_URL = "SourceUrl";
    public static final String AUTHOR = "Author";
    public static final String ARTICLE_DESCRPTION = "ArticleDescription";
    public static final String ARTICLE_TITLE = "ArticleTitle";
    public static final String ARTICLE_URL = "ArticleUrl";
    public static final String ARTICLE_IMAGE_URL = "ArticleImageUrl";
    public static final String PUBLISHED_AT = "PublishedAt";
    public static final String SOURCE_LOGO_URL = "SourceLogo_Url";
}
