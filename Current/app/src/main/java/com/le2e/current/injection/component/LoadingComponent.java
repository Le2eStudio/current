package com.le2e.current.injection.component;

import com.le2e.current.injection.module.LoadingModule;
import com.le2e.current.ui.loading_screen.DataLoadActivity;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component (modules = {LoadingModule.class})
public interface LoadingComponent {
    void inject (DataLoadActivity activity);
}
