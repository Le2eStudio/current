package com.le2e.current.injection.component;

import com.le2e.current.injection.module.TabFragmentModule;
import com.le2e.current.ui.home_screen.tab_fragments.FlowFragment;
import com.le2e.current.ui.home_screen.tab_fragments.RecommendedFragment;
import com.le2e.current.ui.home_screen.tab_fragments.SavedFragment;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {TabFragmentModule.class})
public interface TabFragmentComponent {
    void inject(RecommendedFragment fragment);

    void inject(FlowFragment fragment);

    void inject(SavedFragment fragment);
}
