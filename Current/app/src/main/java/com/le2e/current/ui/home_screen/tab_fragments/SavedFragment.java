package com.le2e.current.ui.home_screen.tab_fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.le2e.current.config.BaseApplication;
import com.le2e.current.ui.home_screen.tab_fragments.presenters.SavedTabPresenter;
import com.le2e.current.R;

import javax.inject.Inject;

public class SavedFragment extends Fragment {
    @Inject
    SavedTabPresenter presenter;

    public SavedFragment(){

    }

    public static SavedFragment newInstance(){
        SavedFragment fragment = new SavedFragment();
        // put bundle save logic here
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((BaseApplication) getActivity().getApplication()).getTabFragmentComponent().inject(this);
        Log.d("FragmentLog", "saved created");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Log.d("FragmentLog", "saved view created");
        return inflater.inflate(R.layout.fragment_test_recommended, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        presenter.attachFragment(this);
    }

    @Override
    public void onDestroyView() {
        presenter.detachFragment();
        super.onDestroyView();
        Log.d("FragmentLog", "saved frag destroyed");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d("FragmentLog", "saved frag paused");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d("FragmentLog", "saved frag stopped");
    }
}
