package com.le2e.current.injection.module;

import com.le2e.current.ui.home_screen.tab_fragments.presenters.FlowTabPresenter;
import com.le2e.current.ui.home_screen.tab_fragments.presenters.RecommendedTabPresenter;
import com.le2e.current.ui.home_screen.tab_fragments.presenters.SavedTabPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class TabFragmentModule {
    // Fragment Presenters
    @Provides
    RecommendedTabPresenter provideRecommendedTabPresenter() {
        return new RecommendedTabPresenter();
    }

    @Provides
    FlowTabPresenter providesFlowTabPresenter() {
        return new FlowTabPresenter();
    }

    @Provides
    SavedTabPresenter providesSavedTabPresenter() {
        return new SavedTabPresenter();
    }
}
