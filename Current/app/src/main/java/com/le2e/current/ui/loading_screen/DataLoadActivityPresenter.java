package com.le2e.current.ui.loading_screen;

import android.os.Handler;

import com.le2e.current.data.manager.DataManager;
import com.le2e.current.data.model.db.ProvisionalDBO;
import com.le2e.current.data.model.db.SourceDBO;
import com.le2e.current.ui.base.BaseActivityPresenter;

import java.util.ArrayList;
import java.util.HashMap;

import timber.log.Timber;

public class DataLoadActivityPresenter extends BaseActivityPresenter implements PresenterImpl {
    private PresenterLoadView presenterLoadView;
    private DataManager dlm;
    private Handler handler;

    private Throwable loadFail;

    private int progressStatus;
    private boolean isDoneFullLoading = false;
    private boolean isDoneLoadingRecommended = true;
    private boolean fullLoadFail = false;
    private final int LOAD_PROGRESS_WAIT_VALUE = 50;
    private final int RECOMMENDED_PROGRESS_WAIT_VALUE = 90;

    public DataLoadActivityPresenter() {
        handler = new Handler();
        dlm = new DataManager(this);
    }

    @Override
    protected void setInterface() {
        if (activityRef != null)
            presenterLoadView = (PresenterLoadView) activityRef.get();
    }

    // ***********************************************************************************
    // ******************************** ACTIVITY REQUESTS ********************************
    // ***********************************************************************************

    void initiateLoad() {
        // start full load sequence with DM
        Timber.d("call to DM to start full load made");
        dlm.startSourceFullLoadSequence();
        updateProgress(1, 100);
    }

    void unsubscribeObserves() {
        dlm.unSubscribeObservables();
    }

    // ***********************************************************************************
    // ******************************** LOADING SEQUENCE *********************************
    // ***********************************************************************************

    @Override
    public void sourceLoadSuccess() {

    }

    @Override
    public void articleLoadSuccess() {
    }

    @Override
    public void sourceRequestSuccess(ArrayList<SourceDBO> list) {

    }

    @Override
    public void provisionalRequestSuccess(ArrayList<ProvisionalDBO> provisionalDBOs) {

    }

    @Override
    public void sourceLogoRequestSuccess(HashMap<String, String> logoList) {

    }

    @Override
    public void fullLoadSuccess() {
        isDoneFullLoading = true;
    }

    // ***********************************************************************************
    // ****************************** LOADING FAIL HANDLERS ******************************
    // ***********************************************************************************

    @Override
    public void getNewsSourceFail() {

    }

    @Override
    public void saveNewsSourceFail() {

    }

    @Override
    public void getArticlesFail() {

    }

    @Override
    public void saveArticlesFail() {

    }

    @Override
    public void sourceRequestFail() {

    }

    @Override
    public void provisionalRequestFail() {

    }

    @Override
    public void sourceLogoRequestFail() {

    }

    @Override
    public void fullLoadFail(Throwable e) {
        fullLoadFail = true;
        loadFail = e;
    }

    // loading utility
    private void updateProgress(int start, final int finish) {
        progressStatus = start;
        new Thread(new Runnable() {
            @Override
            public void run() {
                while (progressStatus < finish) {
                    try{
                        Thread.sleep(50);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    if((!isDoneFullLoading) && progressStatus == LOAD_PROGRESS_WAIT_VALUE)
                        continue;

                    // choke point for recommended article list load
                    if((!isDoneLoadingRecommended) && progressStatus == RECOMMENDED_PROGRESS_WAIT_VALUE)
                        continue;

                    progressStatus += 1;
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            presenterLoadView.onProgressUpdate(progressStatus);
                        }
                    });
                }

                if(fullLoadFail)
                    presenterLoadView.loadFail(loadFail);
                else
                    presenterLoadView.loadingFinish();
            }
        }).start();
    }
}
