package com.le2e.current.ui.home_screen;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.le2e.current.R;
import com.le2e.current.config.BaseApplication;
import com.le2e.current.ui.home_screen.tab_fragments.FlowFragment;
import com.le2e.current.ui.home_screen.tab_fragments.RecommendedFragment;
import com.le2e.current.ui.home_screen.tab_fragments.SavedFragment;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeActivity extends AppCompatActivity {
    @Inject
    HomeActivityPresenter presenter;

    @BindView(R.id.tabLayout)
    TabLayout tabLayout;

    @BindView(R.id.viewPager)
    ViewPager viewPager;

    @BindView(R.id.home_toolbar)
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_home);
        ((BaseApplication) getApplication()).getHomeComponent().inject(this);
        ButterKnife.bind(this);
        presenter.attachActivity(this);

        setSupportActionBar(toolbar);
        setTitle("Current");
        setupAdapter();
        tabLayout.setupWithViewPager(viewPager);
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.attachActivity(this);
        setupTabLayout();
    }

    @Override
    protected void onPause() {
        super.onPause();
        presenter.detachActivity();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getTitle().equals(getResources().getString(R.string.mi_settings_title))) {
            Toast.makeText(this, "Settings clicked", Toast.LENGTH_SHORT).show();
        } else if (item.getTitle().equals(getResources().getString(R.string.mi_refresh_title))){
            Toast.makeText(this, "Refresh clicked", Toast.LENGTH_SHORT).show();
            presenter.refreshNewsContent();
        }
        return true;
    }

    private void setupTabLayout() {
        tabLayout.getTabAt(0).setIcon(R.drawable.ic_whatshot_white_36dp);
        tabLayout.getTabAt(1).setIcon(R.drawable.ic_search_white_36dp);
        tabLayout.getTabAt(2).setIcon(R.drawable.ic_favorite_white_36dp);
        tabLayout.setSelectedTabIndicatorColor(ContextCompat.getColor(this, R.color.test_dark));
    }

    private void setupAdapter() {
        HomeScreenPagerAdapter adapter = new HomeScreenPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(RecommendedFragment.newInstance());
        adapter.addFragment(FlowFragment.newInstance());
        adapter.addFragment(SavedFragment.newInstance());
        viewPager.setAdapter(adapter);
    }
}

