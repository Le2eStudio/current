package com.le2e.current.injection.module;

import com.le2e.current.ui.home_screen.HomeActivityPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class HomeModule {
    @Provides
    HomeActivityPresenter providesHomeActivityPresenter() {
        return new HomeActivityPresenter();
    }
}
