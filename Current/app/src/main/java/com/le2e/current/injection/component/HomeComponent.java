package com.le2e.current.injection.component;

import com.le2e.current.injection.module.HomeModule;
import com.le2e.current.ui.home_screen.HomeActivity;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component (modules = {HomeModule.class})
public interface HomeComponent {
    void inject (HomeActivity activity);
}
