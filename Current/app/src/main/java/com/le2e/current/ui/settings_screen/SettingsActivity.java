package com.le2e.current.ui.settings_screen;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.le2e.current.R;
import com.le2e.current.config.BaseApplication;

import javax.inject.Inject;

import butterknife.ButterKnife;

public class SettingsActivity extends AppCompatActivity {
    @Inject
    SettingsActivityPresenter presenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        ((BaseApplication) getApplication()).getSettingsComponent().inject(this);
        ButterKnife.bind(this);
    }

    @Override
    protected void onResume() {
        presenter.attachActivity(this);
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        presenter.detachActivity();
        super.onDestroy();
    }
}
