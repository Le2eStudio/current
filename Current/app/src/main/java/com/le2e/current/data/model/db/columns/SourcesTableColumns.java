package com.le2e.current.data.model.db.columns;

public class SourcesTableColumns {
    public static final String TABLE = "SourcesTable";
    public static final String _ROW_ID = "RowId";
    public static final String SOURCE_ID = "SourceId";
    public static final String SOURCE_NAME = "SourceName";
    public static final String SOURCE_DESCRIPTION = "SourceDescription";
    public static final String SOURCE_URL = "SourceUrl";
    public static final String SOURCE_CATEGORY = "SourceCategory";
    public static final String SOURCE_LANGUAGE = "SourceLanguage";
    public static final String SOURCE_COUNTRY = "SourceCountry";
    public static final String SOURCE_LOGO_URL = "SourceLogoUrl";

    public String [] getColumns(){
        String [] col = {_ROW_ID, SOURCE_ID, SOURCE_NAME, SOURCE_DESCRIPTION, SOURCE_URL, SOURCE_CATEGORY, SOURCE_LANGUAGE, SOURCE_COUNTRY, SOURCE_LOGO_URL };
        return col;
    }
}
