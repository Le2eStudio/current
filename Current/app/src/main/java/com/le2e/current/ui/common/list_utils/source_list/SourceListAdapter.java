package com.le2e.current.ui.common.list_utils.source_list;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.le2e.current.R;
import com.le2e.current.data.remote.request.newsapi.CurrentImageLoader;
import com.le2e.current.data.remote.response.newsapi.SourceDetails;

import java.util.ArrayList;

public class SourceListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    public static final String TAG = SourceListAdapter.class.getSimpleName();

    private final ArrayList<SourceDetails> listOfSourceDetails;
    private final LayoutInflater inflater;
    private final Context context;

    public SourceListAdapter (Context context){
        listOfSourceDetails = new ArrayList<>();
        this.context = context;
        inflater = LayoutInflater.from(context);
    }

    public void addSourceIconToList(SourceDetails details){
        listOfSourceDetails.add(details);
        notifyItemInserted(listOfSourceDetails.size() - 1);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        view = inflater.inflate(R.layout.list_item_source_icon, parent, false);
        return new SourceIconViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        processSourceIcon((SourceIconViewHolder) holder, position);
    }

    private void processSourceIcon(SourceIconViewHolder holder, int position){
        String imgUrl = listOfSourceDetails.get(position).getUrlsToLogos().getSmall();
        if (!imgUrl.isEmpty()){
            CurrentImageLoader.loadIcon(context, imgUrl, holder.sourceIcon);
        }
    }

    @Override
    public int getItemCount() {
        return listOfSourceDetails.size();
    }
}
