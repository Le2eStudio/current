package com.le2e.current.config;

import android.app.Application;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.le2e.current.BuildConfig;
import com.le2e.current.data.manager.db.DatabaseManager;
import com.le2e.current.injection.component.DaggerHomeComponent;
import com.le2e.current.injection.component.DaggerLoadingComponent;
import com.le2e.current.injection.component.DaggerSettingsComponent;
import com.le2e.current.injection.component.DaggerTabFragmentComponent;
import com.le2e.current.injection.component.HomeComponent;
import com.le2e.current.injection.component.LoadingComponent;
import com.le2e.current.injection.component.SettingsComponent;
import com.le2e.current.injection.component.TabFragmentComponent;

import io.fabric.sdk.android.Fabric;
import timber.log.Timber;

public class BaseApplication extends Application {
    private TabFragmentComponent tabComponent;
    private LoadingComponent loadComponent;
    private HomeComponent homeComponent;
    private SettingsComponent settingsComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        } else {
            Timber.plant(new CrashReportingTree());
        }

        Fabric.with(this, new Crashlytics());

        DatabaseManager.createInstance(this);

        //refactor after network layer
        homeComponent = DaggerHomeComponent.builder()
                .build();

        tabComponent = DaggerTabFragmentComponent.builder()
                .build();

        loadComponent = DaggerLoadingComponent.builder()
                .build();

        settingsComponent = DaggerSettingsComponent.builder()
                .build();
    }

    // *********************************************************************
    // ************************* COMPONENT GETTERS *************************
    // *********************************************************************

    public TabFragmentComponent getTabFragmentComponent() {
        return tabComponent;
    }

    public LoadingComponent getLoadComponent() {
        return loadComponent;
    }

    public HomeComponent getHomeComponent() {
        return homeComponent;
    }

    public SettingsComponent getSettingsComponent() {
        return settingsComponent;
    }

    // ********************************************************************************
    // ************************* TIMBER / CRASHLYTICS REPORTS *************************
    // ********************************************************************************

    private class CrashReportingTree extends Timber.Tree {
        private static final String CRASHLYTICS_KEY_PRIORITY = "priority";
        private static final String CRASHLYTICS_KEY_TAG = "tag";
        private static final String CRASHLYTICS_KEY_MESSAGE = "message";

        @Override
        protected void log(int priority, String tag, String message, Throwable t) {
            if (priority == Log.VERBOSE || priority == Log.DEBUG) {
                return;
            }

            Crashlytics.setInt(CRASHLYTICS_KEY_PRIORITY, priority);
            Crashlytics.setString(CRASHLYTICS_KEY_TAG, tag);
            Crashlytics.setString(CRASHLYTICS_KEY_MESSAGE, message);
            Crashlytics.logException(t);
        }
    }
}
