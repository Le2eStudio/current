package com.le2e.current.data.manager.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.le2e.current.data.local.db.SourceDBAdapter;
import com.le2e.current.data.local.db.ProvisionalDBAdapter;

public class DatabaseManager {
    private static final String TAG = DatabaseManager.class.getSimpleName();

    public static DatabaseManager instance;

    private final Context context;
    private final DBHelper helper;
    private final SQLiteDatabase database;
    private SourceDBAdapter sourceDBAdapter;
    private ProvisionalDBAdapter provisionalDBAdapter;

    private DatabaseManager(Context context) {
        this.context = context;
        helper = new DBHelper(context);
        database = helper.getWritableDatabase();
    }

    public static DatabaseManager createInstance(Context context) {
        instance = new DatabaseManager(context);
        return instance;
    }

    public static DatabaseManager getInstance(){
        return instance;
    }

    public DBHelper getHelper(){
        return helper;
    }

    public SQLiteDatabase getDatabase() {
        return database;
    }

    public SourceDBAdapter getSourceDBAdapter() {
        if(sourceDBAdapter == null)
            sourceDBAdapter = new SourceDBAdapter(this);

        return sourceDBAdapter;
    }

    public ProvisionalDBAdapter getProvisionalDBAdapter(){
        if(provisionalDBAdapter == null)
            provisionalDBAdapter = new ProvisionalDBAdapter(this);

        return provisionalDBAdapter;
    }
}
