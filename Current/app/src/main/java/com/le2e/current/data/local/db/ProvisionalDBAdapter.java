package com.le2e.current.data.local.db;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.util.Log;

import com.le2e.current.data.manager.db.DatabaseManager;
import com.le2e.current.data.model.db.ProvisionalDBO;
import com.le2e.current.data.model.db.columns.ProvisionalTableColumns;
import com.le2e.current.data.remote.response.newsapi.ArticleDetails;

import java.util.ArrayList;

import rx.Observable;
import rx.functions.Func0;
import timber.log.Timber;

public class ProvisionalDBAdapter {
    private static final String TAG = ProvisionalDBAdapter.class.getSimpleName();
    private DatabaseManager dbManager;

    public ProvisionalDBAdapter(DatabaseManager dbm) {
        dbManager = dbm;
    }

    // ***********************************************************************************
    // *********************************** OBSERVABLES ***********************************
    // ***********************************************************************************

    public Observable<ArrayList<ProvisionalDBO>> getAllProvisionalArticlesObserve() {
        return Observable.defer(new Func0<Observable<ArrayList<ProvisionalDBO>>>() {
            @Override
            public Observable<ArrayList<ProvisionalDBO>> call() {
                return Observable.just(getAllFromProvisionalTable());
            }
        });
    }

    public Observable<Boolean> clearProvisionalTableObserve(){
        return Observable.defer(new Func0<Observable<Boolean>>() {
            @Override
            public Observable<Boolean> call() {
                return Observable.just(deleteAllProvisionalArticles());
            }
        });
    }

    public Observable<ProvisionalDBO> insertProvisionalToDBObserve(final ArticleDetails details){
        return Observable.defer(new Func0<Observable<ProvisionalDBO>>() {
            @Override
            public Observable<ProvisionalDBO> call() {
                return Observable.just(saveProvisionalToDB(details));
            }
        });
    }

    public Observable<Long> getProvisionCountObserve(){
        return Observable.defer(new Func0<Observable<Long>>() {
            @Override
            public Observable<Long> call() {
                return Observable.just(getCount());
            }
        });
    }

    public Observable<ArrayList<ProvisionalDBO>> getProsBySingleSourceIdObserve(final String sourceId){
        return Observable.defer(new Func0<Observable<ArrayList<ProvisionalDBO>>>() {
            @Override
            public Observable<ArrayList<ProvisionalDBO>> call() {
                return Observable.just(getProvisionalsBySourceId(sourceId));
            }
        });
    }

    // ***********************************************************************************
    // ************************************* QUERIES *************************************
    // ***********************************************************************************

    // saves 1 passed in article details to new row in provisional table
    private ProvisionalDBO saveProvisionalToDB(ArticleDetails details){
        ContentValues cv = new ContentValues();
        cv.put(ProvisionalTableColumns.SOURCE_ID, details.getSourceId());
        cv.put(ProvisionalTableColumns.AUTHOR, details.getAuthor());
        cv.put(ProvisionalTableColumns.ARTICLE_DESCRIPTION, details.getDescription());
        cv.put(ProvisionalTableColumns.ARTICLE_TITLE, details.getTitle());
        cv.put(ProvisionalTableColumns.ARTICLE_URL, details.getUrl());
        cv.put(ProvisionalTableColumns.ARTICLE_IMAGE_URL, details.getUrlToImage());
        cv.put(ProvisionalTableColumns.PUBLISHED_AT, details.getPublishedAt());
        Long rowId = dbManager.getDatabase().insert(ProvisionalTableColumns.TABLE, null, cv);
        return new ProvisionalDBO(rowId, details);
    }

    // clears all rows in provisional table
    private boolean deleteAllProvisionalArticles() {
        try {
            dbManager.getHelper().clearProvisions(DatabaseManager.getInstance().getDatabase());
            return true;
        } catch (Exception e){
            Timber.d(e, "provisionalAdapter.clearTable() fail");
            return false;
        }
    }

    // returns all rows in provisional table as dbo
    private ArrayList<ProvisionalDBO> getAllFromProvisionalTable() {
        Cursor cursor = null;
        ArrayList<ProvisionalDBO> resultList = new ArrayList<>();

        try {
            String query = "SELECT * FROM " + ProvisionalTableColumns.TABLE;
            cursor = dbManager.getDatabase().rawQuery(query, null);

            int count = cursor.getCount();
            if (count > 0) {
                resultList = popProDBOList(cursor);
            }
        } catch (Exception e) {
            Log.d(TAG, e.toString());
        } finally {
            if (cursor != null)
                cursor.close();
        }

        return resultList;
    }

    // returns the number of rows in provisional table
    private long getCount() {
        return DatabaseUtils.queryNumEntries(dbManager.getDatabase(), ProvisionalTableColumns.TABLE);
    }

    // Get all articles that match one specific source
    private ArrayList<ProvisionalDBO> getProvisionalsBySourceId(String sId) {
        Cursor cursor = null;
        ArrayList<ProvisionalDBO> resultList = new ArrayList<>();

        try {
            String query = "SELECT * FROM " + ProvisionalTableColumns.TABLE + " WHERE " + ProvisionalTableColumns.SOURCE_ID + " = " + "'" + sId + "'";
            cursor = dbManager.getDatabase().rawQuery(query, null);

            int count = cursor.getCount();
            if (count > 0) {
                resultList = popProDBOList(cursor);
            }
        } catch (Exception e) {
            Log.d(TAG, e.toString());
        } finally {
            if (cursor != null)
                cursor.close();
        }

        return resultList;
    }

    // Get all articles that match a set of sources
    private ArrayList<ProvisionalDBO> getProvisionalsBySourceId(ArrayList<String> sourceIds) {
        return null;
    }

    // ***********************************************************************************
    // ************************************ UTILITIES ************************************
    // ***********************************************************************************

    // utility function for filling a list of provisionalDBOs to return to user
    private ArrayList<ProvisionalDBO> popProDBOList(Cursor cursor) {
        ArrayList<ProvisionalDBO> list = new ArrayList<>();
        ProvisionalDBO provisionalDBO;
        ArticleDetails details;

        while (cursor.moveToNext()) {
            int rowId = cursor.getInt(cursor.getColumnIndex(ProvisionalTableColumns._ARTICLE_ID));
            String sourceId = cursor.getString(cursor.getColumnIndex(ProvisionalTableColumns.SOURCE_ID));
            String author = cursor.getString(cursor.getColumnIndex(ProvisionalTableColumns.AUTHOR));
            String description = cursor.getString(cursor.getColumnIndex(ProvisionalTableColumns.ARTICLE_DESCRIPTION));
            String title = cursor.getString(cursor.getColumnIndex(ProvisionalTableColumns.ARTICLE_TITLE));
            String url = cursor.getString(cursor.getColumnIndex(ProvisionalTableColumns.ARTICLE_URL));
            String image_url = cursor.getString(cursor.getColumnIndex(ProvisionalTableColumns.ARTICLE_IMAGE_URL));
            String publishedAt = cursor.getString(cursor.getColumnIndex(ProvisionalTableColumns.PUBLISHED_AT));

            details = new ArticleDetails(author, title, description, url, image_url, publishedAt, sourceId);

            provisionalDBO = new ProvisionalDBO(rowId, details);
            list.add(provisionalDBO);
        }
        return list;
    }
}
