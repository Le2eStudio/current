package com.le2e.current.ui.base;


public interface DataManagerPresenter {
    void fullLoadSuccess();
    void fullLoadFail(Throwable e);
}
