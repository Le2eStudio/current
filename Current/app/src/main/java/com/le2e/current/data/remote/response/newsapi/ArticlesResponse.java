package com.le2e.current.data.remote.response.newsapi;

import com.google.gson.annotations.SerializedName;
import com.le2e.current.data.remote.response.newsapi.ArticleDetails;

import java.util.ArrayList;

public class ArticlesResponse {
    @SerializedName("status")
    private String status;
    @SerializedName("source")
    private String source;
    @SerializedName("sortBy")
    private String sortBy;
    @SerializedName("articles")
    private ArrayList<ArticleDetails> articles;

    public String getStatus() {
        return status;
    }

    public String getSource() {
        return source;
    }

    public String getSortBy() {
        return sortBy;
    }

    public ArrayList<ArticleDetails> getArticles() {
        return articles;
    }
}
