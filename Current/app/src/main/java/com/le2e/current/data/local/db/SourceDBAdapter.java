package com.le2e.current.data.local.db;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.DatabaseUtils;

import com.le2e.current.data.manager.db.DatabaseManager;
import com.le2e.current.data.model.db.SourceDBO;
import com.le2e.current.data.model.db.columns.SourcesTableColumns;
import com.le2e.current.data.remote.response.newsapi.SourceDetails;

import java.util.ArrayList;
import java.util.HashMap;

import rx.Observable;
import rx.functions.Func0;
import timber.log.Timber;

public class SourceDBAdapter {
    private DatabaseManager dbManager;

    public SourceDBAdapter(DatabaseManager dbm) {
        dbManager = dbm;
    }

    // ***********************************************************************************
    // *********************************** OBSERVABLES ***********************************
    // ***********************************************************************************

    public Observable<ArrayList<SourceDBO>> getAllSourcesObserve() {
        return Observable.defer(new Func0<Observable<ArrayList<SourceDBO>>>() {
            @Override
            public Observable<ArrayList<SourceDBO>> call() {
                return Observable.just(getAllNewsSources());
            }
        });
    }

    public Observable<Long> getSourceTableCountObserve() {
        return Observable.defer(new Func0<Observable<Long>>() {
            @Override
            public Observable<Long> call() {
                return Observable.just(getCount());
            }
        });
    }

    public Observable<HashMap<String, String>> getLogosObserve() {
        return Observable.defer(new Func0<Observable<HashMap<String, String>>>() {
            @Override
            public Observable<HashMap<String, String>> call() {
                return Observable.just(getSourceLogos());
            }
        });
    }

    public Observable<SourceDBO> insertSourceToDBObserve(final SourceDetails details){
        return Observable.defer(new Func0<Observable<SourceDBO>>() {
            @Override
            public Observable<SourceDBO> call() {
                return Observable.just(saveSourceToDB(details));
            }
        });
    }

    public Observable<Boolean> clearSourceTableObserve(){
        return Observable.defer(new Func0<Observable<Boolean>>() {
            @Override
            public Observable<Boolean> call() {
                return Observable.just(deleteAllSources());
            }
        });
    }

    // ***********************************************************************************
    // ************************************* QUERIES *************************************
    // ***********************************************************************************

    // saves 1 passed in source detail to source table
    private SourceDBO saveSourceToDB(SourceDetails source){
        ContentValues cv = new ContentValues();
        cv.put(SourcesTableColumns.SOURCE_ID, source.getId());
        cv.put(SourcesTableColumns.SOURCE_NAME, source.getName());
        cv.put(SourcesTableColumns.SOURCE_DESCRIPTION, source.getDescription());
        cv.put(SourcesTableColumns.SOURCE_URL, source.getUrl());
        cv.put(SourcesTableColumns.SOURCE_CATEGORY, source.getCategory());
        cv.put(SourcesTableColumns.SOURCE_LANGUAGE, source.getLanguage());
        cv.put(SourcesTableColumns.SOURCE_COUNTRY, source.getCountry());
        cv.put(SourcesTableColumns.SOURCE_LOGO_URL, source.getUrlsToLogos().getSmall());
        Long rowid = dbManager.getDatabase().insert(SourcesTableColumns.TABLE, null, cv);
        return new SourceDBO(rowid, source);
    }

    // returns all source rows from source table as dbo
    private ArrayList<SourceDBO> getAllNewsSources() {
        Cursor cursor = null;
        ArrayList<SourceDBO> dboList = new ArrayList<>();
        SourceDBO sourceDBO;
        SourceDetails details;

        try {
            String query = "SELECT * FROM " + SourcesTableColumns.TABLE;
            cursor = dbManager.getDatabase().rawQuery(query, null);

            int count = cursor.getCount();
            if (count > 0) {
                while (cursor.moveToNext()) {
                    Long rowId = cursor.getLong(cursor.getColumnIndex(SourcesTableColumns._ROW_ID));
                    String sourceId = cursor.getString(cursor.getColumnIndex(SourcesTableColumns.SOURCE_ID));
                    String name = cursor.getString(cursor.getColumnIndex(SourcesTableColumns.SOURCE_NAME));
                    String description = cursor.getString(cursor.getColumnIndex(SourcesTableColumns.SOURCE_DESCRIPTION));
                    String url = cursor.getString(cursor.getColumnIndex(SourcesTableColumns.SOURCE_URL));
                    String cat = cursor.getString(cursor.getColumnIndex(SourcesTableColumns.SOURCE_CATEGORY));
                    String lang = cursor.getString(cursor.getColumnIndex(SourcesTableColumns.SOURCE_LANGUAGE));
                    String country = cursor.getString(cursor.getColumnIndex(SourcesTableColumns.SOURCE_COUNTRY));
                    String logoUrl = cursor.getString(cursor.getColumnIndex(SourcesTableColumns.SOURCE_LOGO_URL));

                    details = new SourceDetails(sourceId, name, description, url, cat, lang, country, logoUrl);

                    sourceDBO = new SourceDBO(rowId, details);
                    dboList.add(sourceDBO);
                }
            }
        } catch (Exception e) {
            Timber.d(e.toString());
        } finally {
            if (cursor != null)
                cursor.close();
        }

        return dboList;
    }

    // returns number of rows in source table
    private long getCount() {
        return DatabaseUtils.queryNumEntries(dbManager.getDatabase(), SourcesTableColumns.TABLE);
    }

    // clears all rows in source table
    private boolean deleteAllSources() {
        try {
            dbManager.getHelper().clearSources(DatabaseManager.getInstance().getDatabase());
            return true;
        } catch (Exception e){
            Timber.d(e, "dbAdapter.deleteAllSources() fail");
            return false;
        }
    }

    // returns all sources and their respective logo urls in a hash map
    private HashMap<String, String> getSourceLogos() {
        Cursor cursor = null;
        HashMap<String, String> list = new HashMap<>();

        try {
            String query = "SELECT " + SourcesTableColumns.SOURCE_LOGO_URL + ", " + SourcesTableColumns.SOURCE_ID + " FROM " + SourcesTableColumns.TABLE;
            cursor = dbManager.getDatabase().rawQuery(query, null);

            int count = cursor.getCount();
            if (count > 0) {
                while (cursor.moveToNext()) {
                    String id = cursor.getString(cursor.getColumnIndex(SourcesTableColumns.SOURCE_ID));
                    String url = cursor.getString(cursor.getColumnIndex(SourcesTableColumns.SOURCE_LOGO_URL));

                    if (!list.containsKey(id))
                        list.put(id, url);
                }
            }
        } catch (Exception e) {
            Timber.d(e.toString());
        } finally {
            if (cursor != null)
                cursor.close();
        }

        return list;
    }

    // ***********************************************************************************
    // ************************************ UTILITIES ************************************
    // ***********************************************************************************
}
