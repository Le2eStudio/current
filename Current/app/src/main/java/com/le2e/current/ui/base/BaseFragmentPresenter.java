package com.le2e.current.ui.base;

import android.support.v4.app.Fragment;

import java.lang.ref.WeakReference;

public class BaseFragmentPresenter {
    protected WeakReference<Fragment> fragmentRef;

    public void attachFragment(Fragment fragment){
        fragmentRef = new WeakReference<>(fragment);
    }

    public void detachFragment(){
        fragmentRef = null;
    }
}
