package com.le2e.current.data.remote.response.newsapi;

import com.google.gson.annotations.SerializedName;

public class urlsToLogos {
    @SerializedName("small")
    private String small;
    @SerializedName("medium")
    private String medium;
    @SerializedName("large")
    private String large;

    public String getSmall() {
        return small;
    }

    public String getMedium() {
        return medium;
    }

    public String getLarge() {
        return large;
    }
}
