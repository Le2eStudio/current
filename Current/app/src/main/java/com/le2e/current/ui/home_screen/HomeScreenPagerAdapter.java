package com.le2e.current.ui.home_screen;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.ArrayList;

public class HomeScreenPagerAdapter extends FragmentStatePagerAdapter {
    private final ArrayList<Fragment> listOfFragments;

    public HomeScreenPagerAdapter(FragmentManager supportFragmentManager) {
        super(supportFragmentManager);
        listOfFragments = new ArrayList<>();
    }

    public void addFragment(Fragment fragment) {
        listOfFragments.add(fragment);
    }

    @Override
    public Fragment getItem(int position) {
        return listOfFragments.get(position);
    }

    @Override
    public int getCount() {
        return listOfFragments.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return null;
    }
}
