package com.le2e.current.ui.common.list_utils.article_list;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.support.customtabs.CustomTabsIntent;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.le2e.current.R;
import com.le2e.current.data.model.db.ProvisionalDBO;
import com.le2e.current.data.remote.request.newsapi.CurrentImageLoader;
import com.le2e.current.data.remote.response.newsapi.ArticleDetails;
import com.le2e.current.ui.loading_screen.PresenterImpl;

import java.util.ArrayList;
import java.util.HashMap;

import timber.log.Timber;

public class ArticleListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final ArrayList<ProvisionalDBO> articleList;
    private final LayoutInflater inflater;
    private final Context context;
    private final PresenterImpl presenter;
    private HashMap<String, String> logoList;

    public ArticleListAdapter(Context context, PresenterImpl presenter) {
        articleList = new ArrayList<>();
        logoList = new HashMap<>();
        this.presenter = presenter;
        this.context = context;
        inflater = LayoutInflater.from(context);
    }

    public void setLogoList(HashMap<String, String> logoList) {
        this.logoList.clear();
        this.logoList = logoList;
    }

    public void addArticleDetailToList(ProvisionalDBO details) {
        articleList.add(details);
        notifyItemInserted(articleList.size() - 1);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.list_item_article_card, parent, false);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ArticleDetails details = (ArticleDetails) view.getTag();
                openChromeTab(details.getUrl());
            }
        });
        return new ArticleDetailViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        processArticleDetails((ArticleDetailViewHolder) holder, position);
    }

    private void processArticleDetails(ArticleDetailViewHolder holder, int position) {
        ArticleDetails details = articleList.get(position).details;
        holder.itemView.setTag(details);

        if (details.getTitle() != null)
            holder.title.setText(details.getTitle());

        if (details.getAuthor() != null)
            holder.author.setText(details.getAuthor());

        if (details.getDescription() != null)
            holder.description.setText(details.getDescription());

        if (details.getPublishedAt() != null)
            holder.publishedDate.setText(details.getPublishedAt());

        if (details.getSourceId() != null) {
            String url = logoList.get(details.getSourceId());
            if (url != null && !url.isEmpty()) {
                CurrentImageLoader.loadImage(context, logoList.get(details.getSourceId()), holder.logo);
                Timber.d("logo url: %s", logoList.get(details.getSourceId()));
            }
        }

        // add in images with picasso
        if (details.getUrlToImage() != null)
            CurrentImageLoader.loadImage(context, details.getUrlToImage(), holder.image);
    }

    private void openChromeTab(String url) {
        Uri uri = Uri.parse(url);
        CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder();
        builder.setToolbarColor(ContextCompat.getColor(context, R.color.colorPrimary));
        builder.setSecondaryToolbarColor(ContextCompat.getColor(context, R.color.colorPrimaryDark));

        CustomTabsIntent intent = builder.build();
        intent.launchUrl((Activity) context, uri);
    }

    @Override
    public int getItemCount() {
        return articleList.size();
    }
}
