package com.le2e.current.ui.loading_screen;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.le2e.current.R;
import com.le2e.current.config.BaseApplication;
import com.le2e.current.ui.home_screen.HomeActivity;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import timber.log.Timber;

public class DataLoadActivity extends AppCompatActivity implements PresenterLoadView {
    @Inject
    DataLoadActivityPresenter presenter;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.loading_progress_text)
    TextView progressTextView;

    @BindView(R.id.tempbuton)
    Button b1;
    @BindView(R.id.tempText)
    TextView txt;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loading_screen);
        ((BaseApplication) getApplication()).getLoadComponent().inject(this);
        ButterKnife.bind(this);
        presenter.attachActivity(this);

        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txt.setText("*** Full Load Start ***");
                presenter.initiateLoad();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.attachActivity(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.detachActivity();
    }

    @Override
    protected void onPause() {
        super.onPause();
        presenter.unsubscribeObserves();
    }

    // *********************************************************************
    // ************************* LOADING CALLBACKS *************************
    // *********************************************************************

    @Override
    public void loadingFinish() {
        Timber.d("Loading screen finished");
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                txt.setText("Full Load Finished");
            }
        });

        startActivity(new Intent(this, HomeActivity.class));
    }

    @Override
    public void loadFail(Throwable e) {
        Timber.e(e, "Full load fail");
    }

    @Override
    public void onProgressUpdate(int i) {
        progressBar.setProgress(i);
        String s = i + "%";
        progressTextView.setText(s);
    }
}
