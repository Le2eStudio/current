package com.le2e.current.ui.loading_screen;

import com.le2e.current.data.remote.response.newsapi.ArticleDetails;
import com.le2e.current.data.remote.response.newsapi.SourceDetails;

import java.util.ArrayList;

public interface DataLoadCallbacks {
    void getArticlesApiSuccess(ArrayList<ArticleDetails> aList);
    void getArticlesApiFail();

    void getSourcesApiSuccess(ArrayList<SourceDetails> sList);
    void getSourcesApiFailure();

    void getArticlesDBSuccess();
    void getArticlesDBFailure();

    void getSourcesDBSuccess();
    void getSourcesDBFailure();
}
