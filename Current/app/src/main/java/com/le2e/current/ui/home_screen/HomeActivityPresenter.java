package com.le2e.current.ui.home_screen;

import com.le2e.current.data.manager.DataManager;
import com.le2e.current.data.model.db.ProvisionalDBO;
import com.le2e.current.data.model.db.SourceDBO;
import com.le2e.current.ui.base.BaseActivityPresenter;
import com.le2e.current.ui.loading_screen.PresenterImpl;

import java.util.ArrayList;
import java.util.HashMap;

import timber.log.Timber;

public class HomeActivityPresenter extends BaseActivityPresenter implements PresenterImpl{
    private DataManager dm;

    public HomeActivityPresenter(){
        dm = new DataManager(this);
    }

    @Override
    protected void setInterface() {

    }

    void refreshNewsContent(){
        Timber.d("Full load refresh started");
        // show a loading overlay
        dm.startSourceFullLoadSequence();
    }

    @Override
    public void sourceLoadSuccess() {

    }

    @Override
    public void articleLoadSuccess() {

    }

    @Override
    public void getNewsSourceFail() {

    }

    @Override
    public void saveNewsSourceFail() {

    }

    @Override
    public void getArticlesFail() {

    }

    @Override
    public void saveArticlesFail() {

    }

    @Override
    public void sourceRequestSuccess(ArrayList<SourceDBO> sourceDBOs) {

    }

    @Override
    public void sourceRequestFail() {

    }

    @Override
    public void provisionalRequestSuccess(ArrayList<ProvisionalDBO> provisionalDBOs) {

    }

    @Override
    public void provisionalRequestFail() {

    }

    @Override
    public void sourceLogoRequestSuccess(HashMap<String, String> logoList) {

    }

    @Override
    public void sourceLogoRequestFail() {

    }

    @Override
    public void fullLoadSuccess() {
        // dismiss loading overlay
        Timber.d("Full load refresh success");
    }

    @Override
    public void fullLoadFail(Throwable e) {
        // dismiss loading overlay
        Timber.d("Full load refresh fail");
    }
}
