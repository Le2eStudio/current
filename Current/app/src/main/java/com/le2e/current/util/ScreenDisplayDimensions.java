package com.le2e.current.util;

import android.app.Activity;
import android.util.DisplayMetrics;
import android.view.Display;

import java.lang.ref.WeakReference;

public class ScreenDisplayDimensions {
    public static final String TAG = ScreenDisplayDimensions.class.getSimpleName();
    private static ScreenDisplayDimensions instance;

    private WeakReference<Activity> activity;
    private Display display;
    private DisplayMetrics displayMetrics;

    private float density;
    private float height;
    private float width;

    private ScreenDisplayDimensions(WeakReference<Activity> activity) {
        this.activity = activity;
        displayMetrics = new DisplayMetrics();
        determineScreenDimensions();
    }

    public static ScreenDisplayDimensions getInstance(WeakReference<Activity> activity) {
        if (instance == null)
            instance = new ScreenDisplayDimensions(activity);

        return instance;
    }

    private void determineScreenDimensions() {
        display = activity.get().getWindowManager().getDefaultDisplay();
        display.getMetrics(displayMetrics);

        density = displayMetrics.density;
        height = displayMetrics.heightPixels / density;
        width = displayMetrics.widthPixels / density;
    }

    public float getDensity() {
        return density;
    }

    public float getHeight() {
        return height;
    }

    public float getWidth() {
        return width;
    }
}
