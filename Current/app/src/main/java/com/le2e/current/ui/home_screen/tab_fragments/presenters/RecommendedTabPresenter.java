package com.le2e.current.ui.home_screen.tab_fragments.presenters;

import android.support.v7.widget.RecyclerView;

import com.le2e.current.data.manager.DataManager;
import com.le2e.current.data.model.db.ProvisionalDBO;
import com.le2e.current.data.model.db.SourceDBO;
import com.le2e.current.ui.base.BaseFragmentPresenter;
import com.le2e.current.ui.common.list_utils.article_list.ArticleListAdapter;
import com.le2e.current.ui.loading_screen.PresenterImpl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import timber.log.Timber;

public class RecommendedTabPresenter extends BaseFragmentPresenter implements PresenterImpl {

    private DataManager dm;
    private RecyclerView articleListView;
    private ArticleListAdapter listAdapter;
    private ArrayList<ProvisionalDBO> provisionalList;

    public RecommendedTabPresenter() {
        dm = new DataManager(this);
    }

    public void load(RecyclerView recyclerView, ArticleListAdapter adapter) {
        articleListView = recyclerView;
        listAdapter = adapter;

        dm.requestSourceLogos();
    }

    private void populateList() {
        dm.requestProvisionalsFromDB();
    }

    private ArrayList<Integer> randomArticleLogic(int numArticles) {
        ArrayList<Integer> list = new ArrayList<>();
        Random rng = new Random();
        int maxArticles;
        int temp;

        if (numArticles >= 10) {
            maxArticles = 10;
        } else
            maxArticles = numArticles;

        for (int i = 0; i < maxArticles; ++i) {
            boolean dupe = false;

            do {
                temp = rng.nextInt(numArticles);

                for (Integer id : list) {
                    if (id == temp)
                        dupe = true;
                }

                if (!dupe) {
                    list.add(temp);
                    dupe = false;
                }
            } while (dupe);
        }

        return list;
    }

    private void displayRandom10() {
        ArrayList<Integer> articleIds = randomArticleLogic(provisionalList.size());

        for (Integer id : articleIds) {
            listAdapter.addArticleDetailToList(provisionalList.get(id));
        }
    }

    @Override
    public void sourceLoadSuccess() {

    }

    @Override
    public void articleLoadSuccess() {

    }

    @Override
    public void getNewsSourceFail() {

    }

    @Override
    public void saveNewsSourceFail() {

    }

    @Override
    public void getArticlesFail() {

    }

    @Override
    public void saveArticlesFail() {

    }

    @Override
    public void sourceRequestSuccess(ArrayList<SourceDBO> sourceDBOs) {

    }

    @Override
    public void sourceRequestFail() {

    }

    @Override
    public void provisionalRequestSuccess(ArrayList<ProvisionalDBO> provisionalDBOs) {
        provisionalList = provisionalDBOs;
        displayRandom10();
    }

    @Override
    public void provisionalRequestFail() {

    }

    @Override
    public void sourceLogoRequestSuccess(HashMap<String, String> logoList) {
        Timber.d("got logos success");
        listAdapter.setLogoList(logoList);
        populateList();
    }

    @Override
    public void sourceLogoRequestFail() {
        Timber.d("got logos fail");
    }

    @Override
    public void fullLoadSuccess() {

    }

    @Override
    public void fullLoadFail(Throwable e) {

    }
}
