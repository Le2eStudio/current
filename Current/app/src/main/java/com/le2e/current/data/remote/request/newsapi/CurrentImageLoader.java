package com.le2e.current.data.remote.request.newsapi;

import android.content.Context;
import android.widget.ImageView;

import com.le2e.current.R;
import com.squareup.picasso.Picasso;

public class CurrentImageLoader {
    public static void loadImage(Context context, String url, ImageView imageView){
        Picasso.with(context)
                .setIndicatorsEnabled(false);

        Picasso.with(context)
                .load(url)
                .resize(270,135)
                .into(imageView);
    }

    public static void loadIcon(Context context, String url, ImageView imageView){
        Picasso.with(context)
                .setIndicatorsEnabled(false);

        Picasso.with(context)
                .load(url)
                .placeholder(R.mipmap.ic_launcher)
                .into(imageView);
    }
}
