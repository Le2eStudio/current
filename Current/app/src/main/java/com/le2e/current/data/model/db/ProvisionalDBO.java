package com.le2e.current.data.model.db;

import com.le2e.current.data.remote.response.newsapi.ArticleDetails;

public class ProvisionalDBO {
    public long rowId;
    public ArticleDetails details;

    public ProvisionalDBO(long rowId, ArticleDetails details){
        this.rowId = rowId;
        this.details = details;
    }
}
