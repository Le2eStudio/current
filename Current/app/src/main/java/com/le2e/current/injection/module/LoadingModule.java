package com.le2e.current.injection.module;

import com.le2e.current.ui.loading_screen.DataLoadActivityPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class LoadingModule {
    @Provides
    DataLoadActivityPresenter provideDataLoadPresenter() {
        return new DataLoadActivityPresenter();
    }
}
