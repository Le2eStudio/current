package com.le2e.current.ui.loading_screen;

public interface PresenterLoadView {
    void loadingFinish();
    void loadFail(Throwable e);
    void onProgressUpdate(int i);
}
