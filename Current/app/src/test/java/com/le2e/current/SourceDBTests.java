package com.le2e.current;


import org.junit.Test;

public class SourceDBTests {
    String sourcesJsonResponseMock =  "{'status': 'ok','sources': [{'id': 'abc-news-au','name': 'ABC News (AU)','description': 'Australia's most trusted source of local, national and world news. Comprehensive, independent, in-depth analysis, the latest business, sport, weather and more.','url': 'http://www.abc.net.au/news','category': 'general','language': 'en','country': 'au','urlsToLogos': {'small': 'http://i.newsapi.org/abc-news-au-s.png','medium': 'http://i.newsapi.org/abc-news-au-m.png','large': 'http://i.newsapi.org/abc-news-au-l.png'},'sortBysAvailable': ['top']},{'id': 'ars-technica','name': 'Ars Technica','description': 'The PC enthusiast's resource. Power users and the tools they love, without computing religion.','url': 'http://arstechnica.com','category': 'technology','language': 'en','country': 'us','urlsToLogos': {'small': 'http://i.newsapi.org/ars-technica-s.png','medium': 'http://i.newsapi.org/ars-technica-m.png','large': 'http://i.newsapi.org/ars-technica-l.png'},'sortBysAvailable': ['top','latest']},{'id': 'associated-press','name': 'Associated Press','description': 'The AP delivers in-depth coverage on the international, politics, lifestyle, business, and entertainment news.','url': 'http://bigstory.ap.org','category': 'general','language': 'en','country': 'us','urlsToLogos': {'small': 'http://i.newsapi.org/associated-press-s.png','medium': 'http://i.newsapi.org/associated-press-m.png','large': 'http://i.newsapi.org/associated-press-l.png'},'sortBysAvailable': ['top','latest']}}";

    @Test
    public void saveSourcesTest(){

    }

    @Test
    public void retrieveSourcesTest(){

    }

    @Test
    public void clearSourcesTest(){

    }
}
